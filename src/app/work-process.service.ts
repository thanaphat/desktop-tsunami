import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { WorkProcessModel, ListWorkProcessModel
	, PartStructureModel, ProductionTransactionModel } from './work-process.model';
import { Dashboard, JobOutput, TimeProcess } from './dashboard.model';


@Injectable()
export class WorkProcessService {

	private _postJobNoUrl: string = "http://10.152.18.240:3010/JOBNO";
	
	private _postWPUrl: string = "http://10.152.18.240:3010/PDRECORD";
	
	private _postIssuedUrl: string = "http://10.152.18.240:3111/Issued";
	
	private _getJobOnProcess: string = "http://10.152.18.240:3010/checkjob";
	
	private _postBreakState: string = "http://10.152.18.240:3111/MSG";
	
	private _postMCStatus: string = "http://10.152.18.240:3111/mcstatus";
	
	private _postMoveJob: string = "http://10.152.18.240:3015/movejob";
	
	private _postSearchPs: string = "http://10.152.18.240:5000/searchpartstructurebyitemcd";
	
	private _postInsertPdTr: string = "http://10.152.18.240:5000/insertpdtr";
	
	constructor(private http: Http){}
	
	postJobNo(jobNo: string): Observable<WorkProcessModel[]>{
		return this.http
			.post(this._postJobNoUrl, {
				jobno: jobNo != null ? jobNo.trim() : '-'
			})
			.map(res => {
				return res.json();
			})
            .catch(this.handleError);
	}
	
	postWPStart(param1: string, prgid: string): Observable<any[]>{
		return this.http
			.post(this._postWPUrl, {
				param1:	param1 != null ? param1.trim() : '-',
				prgid:	prgid != null ? prgid.trim() : '-'
			})
			.map(res => {
				return res.json();
			})
            .catch(this.handleError);
	}
	
	postWPStop(param1: string, prgid: string): Observable<any[]>{
		return this.http
			.post(this._postWPUrl, {
				param1:	param1 != null ? param1.trim() : '-',
				prgid:	prgid != null ? prgid.trim() : '-'
			})
			.map(res => {
				return res.json();
			})
            .catch(this.handleError);
	}
	
	postIssuedPlanByMachine(date: string): Observable<ListWorkProcessModel[]>{
		return this.http
			.post(this._postIssuedUrl, {
				dts: date != null ? date.trim() : '-'
			})
			.map(res => {
				return res.json();
			})
			.catch(this.handleError);
	}
	
	getJobOnProcess(): Observable<WorkProcessModel[]>{
		return this.http
			.post(this._getJobOnProcess, {})
			.map(res => {
				return res.json();
			})
			.catch(this.handleError);
	}
	
	postProcessState(jobno: string, type: string, emp: string): Observable<any[]>{
		return this.http
			.post(this._postBreakState, {
				jobno: jobno != null ? jobno.trim() : '-',
				type: type != null ? type.trim() : '-',
				emp: emp != null ? emp.trim() : '-'
			})
			.map(res => {
				return res.json();
			})
			.catch(this.handleError);
	}
	
	postMCStatus(dateS: string, dateE: string): Observable<any[]>{
		return this.http
			.post(this._postMCStatus, {
				dts: dateS != null ? dateS.trim() : '1999-01-01',
				dte: dateE != null ? dateE.trim() : '1999-01-01'
			})
			.map(res => {
				return res.json();
			})
			.catch(this.handleError);
	}
	
	postMoveJob(jobno: string, emp: string): Observable<boolean>{
		return this.http
				.post(this._postMoveJob, {
					jobno: jobno,
					empcd: emp
				})
				.map(res => {
					return res.json();
				})
				.catch(this.handleError);
	}
	
	postSearchPsByItemCd(i_fac_cd: string, i_item_cd: string): Observable<PartStructureModel[]>{
		return this.http
				.post(this._postSearchPs, {
					i_fac_cd: i_fac_cd,
					i_sim_item_cd: i_item_cd
				})
				.map(res => {
					return res.json();
				})
				.catch(this.handleError);
	}
	
	postInsertPdTransaction(tr: ProductionTransactionModel): Observable<boolean>{
		return this.http
				.post(this._postInsertPdTr, {
					stkprd: tr.stkprd.trim(),
					whcd: tr.whcd.trim(),
					otpartycd: tr.otpartycd.trim(),
					item_cd: tr.item_cd.trim(),
					customer: tr.customer.trim(),
					cus_partno: tr.cus_partno.trim(),
					packaging: tr.packaging.trim(),
					doctype: tr.doctype.trim(),
					iotype: tr.iotype,
					ioqty: tr.ioqty,
					lotno: tr.lotno.trim(),
					planno: tr.planno.trim(),
					refno1: tr.refno1.trim(),
					refno1_no: tr.refno1_no,
					remark: tr.remark.trim(),
					insprgid: tr.insprgid.trim(),
					insusrid: tr.insusrid.trim()
				})
				.map(res => {
					return res.json();
				})
				.catch(this.handleError);
	}
	
	private handleError(error: Response){
		return Observable.throw(error.statusText);
	}

}