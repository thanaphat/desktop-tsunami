import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuedPlanSearchComponent } from './issued-plan-search.component';

describe('IssuedPlanSearchComponent', () => {
  let component: IssuedPlanSearchComponent;
  let fixture: ComponentFixture<IssuedPlanSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuedPlanSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuedPlanSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
