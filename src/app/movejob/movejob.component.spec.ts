import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovejobComponent } from './movejob.component';

describe('MovejobComponent', () => {
  let component: MovejobComponent;
  let fixture: ComponentFixture<MovejobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovejobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovejobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
