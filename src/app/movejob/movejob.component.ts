import { Component, OnInit, Inject
		, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { WorkProcessService } from '../work-process.service';
import { WorkProcessCmmService } from '../work-process-cmm.service';

@Component({
  selector: 'app-movejob',
  templateUrl: './movejob.component.html',
  styleUrls: ['./movejob.component.css']
})
export class MovejobComponent implements OnInit {

  jobno: string;
  empno: string;
  result: boolean = false;
  
  constructor(private workProcessService: WorkProcessService
			, public dialog: MatDialog
			, private workProcessCmmService: WorkProcessCmmService) { }

  ngOnInit() {
	this.workProcessCmmService.clear.subscribe(state => {
		if(state === true)
			this.clear();
	});
  }
  
  moveJob(){
	if(this.jobno && this.empno){
		return this.workProcessService
			.postMoveJob(this.jobno, this.empno)
			.subscribe(
				resultBoolean => {
					console.log("resultBoolean :: " + resultBoolean['result']);
					this.result = resultBoolean['result'];
					
					if(this.result === true)
						this.workProcessCmmService.submitMoveJob(this.jobno);
					else
						this.openDialog();
						
					this.clear();
				},
				error => console.log("Error :: " + error)
			);
	}
  }
  
  clear(){
	this.jobno = null;
	this.empno = null;
	this.result = false;
  }
  
  openDialog(){
	const dialogConfig = new MatDialogConfig();
	dialogConfig.width = '300px';
	dialogConfig.data = {
		result: this.result,
		title: (this.result === true) ? 'Move job :: ' + this.jobno + ' success.' : 'Move job :: ' + this.jobno + ' not success.'
	}
	
	let dialogRef = this.dialog.open(MoveJobDialogComponent, dialogConfig);
	
	dialogRef.afterClosed().subscribe(p => {
		this.clear();
	});
  }

}

@Component({
	selector: 'app-movejob-dialog',
	templateUrl: './movejob-dialog.component.html',
})
export class MoveJobDialogComponent{
	
	title: string;
	result: boolean;
	
	constructor(private dialogRef: MatDialogRef<MoveJobDialogComponent>
			, @Inject(MAT_DIALOG_DATA) data){
			
		this.title = data.title;
		this.result = data.result;
	}

	close(){
		this.dialogRef.close();
	}
}
