import { Component, OnInit, Input, 
		OnChanges, SimpleChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-stop-watch',
  templateUrl: './stop-watch.component.html',
  styleUrls: ['./stop-watch.component.css']
})
export class StopWatchComponent implements OnChanges {
	
	@Input('stage')
	stage: string = null;
	
	@Input('timeStartTimer')
	start_timer: number = 0;

	ticks: number = 0;
	cur_timer: number = 0;
    
    minutesDisplay: number = 0;
    hoursDisplay: number = 0;
    secondsDisplay: number = 0;

    sub: Subscription;
	
	constructor() { }

	ngOnChanges(changes: SimpleChanges) {
		if(this.stage == 'Start'){
			console.log("stop watch stage :: start");
			this.startTimer();
		}else if(this.stage == 'Stop'){
			console.log("stop watch stage :: stop");
			this.stopTimer();
		}else if(this.stage == 'Reset'){
			this.resetTimer();
		}else if(this.stage == 'Resume'){
			console.log("stopWatch 'start_timer' :: " + this.start_timer);
			this.startTimer();
		}
	}
	
	private startTimer() {
        let timer = Observable.timer(1, 1000);
        this.sub = timer.subscribe(
            t => {
                this.ticks = t;
                this.cur_timer = this.start_timer + this.ticks;
				
                this.secondsDisplay = this.getSeconds(this.cur_timer);
                this.minutesDisplay = this.getMinutes(this.cur_timer);
                this.hoursDisplay = this.getHours(this.cur_timer);
            }
        );
    }
	
	private stopTimer(){
		if(this.sub != null)
			this.sub.unsubscribe();
	}
	
	private resetTimer(){
		this.ticks = 0;
		this.start_timer = 0;
		
		this.hoursDisplay = 0;
		this.minutesDisplay = 0;
		this.secondsDisplay = 0;
	}

    private getSeconds(ticks: number) {
        return this.pad(ticks % 60);
    }

    private getMinutes(ticks: number) {
         return this.pad((Math.floor(ticks / 60)) % 60);
    }

    private getHours(ticks: number) {
        return this.pad(Math.floor((ticks / 60) / 60));
    }

    private pad(digit: any) { 
        return digit <= 9 ? '0' + digit : digit;
    }
	
	showHours(): string{
		return this.hoursDisplay ? this.hoursDisplay.toString() : '00'
	}
	
	showMinutes(): string{
		return (this.minutesDisplay) && (this.minutesDisplay <= 59) ? this.minutesDisplay.toString() : '00';
	}
	
	showSeconds(): string{
		return (this.secondsDisplay) && (this.secondsDisplay <= 59) ? this.secondsDisplay.toString() : '00'
	}

}