export class Dashboard {
	mcp: string;
	date: string;
	shift: string;
	result: {};
	
	constructor(values: Object = {}){
		this.result = [];
		Object.assign(this, values);
	}
}

export class JobOutput{
	jobno: string;
	i_po_detail_no: string;
	i_sim_po_qty: number;
	i_ind_content: string;
	client_name: string;
	time: TimeProcess;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}

export class TimeProcess {
	prc_start: string;
	prc_end: string;
	h_start: string;
	h_end: string;
	state: string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}