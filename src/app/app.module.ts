import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxFitTextModule } from 'ngx-fit-text';
import { FlexLayoutModule } from '@angular/flex-layout';

import { IssuePlanService } from './issue-plan.service';
import { WorkProcessService } from './work-process.service';
import { MatInputModule
		, MatFormFieldModule
		, MatDividerModule
		, MatIconModule
		, MatTabsModule
		, MatSlideToggleModule
		, MatCardModule
		, MatMenuModule
		, MatButtonToggleModule
		, MatButtonModule
		, MatTooltipModule
		, MatTableModule
		, MatRadioModule
		, MatDialogModule, MatDialog
} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

/*
import { MaterialModule } from '@angular/material';
,
	/MaterialModule.forRoot()
*/

import { AppComponent } from './app.component';
import { WorkProcessInputComponent } from './work-process-input/work-process-input.component';
import { ShowWorkProcessDetailComponent } from './show-work-process-detail/show-work-process-detail.component';
import { WorkProcessComponent } from './work-process/work-process.component';
import { ActionWorkProcessComponent } from './action-work-process/action-work-process.component';
import { IssuePlanConfirmComponent } from './issue-plan-confirm/issue-plan-confirm.component';
import { IssuePlanComponent } from './issue-plan/issue-plan.component';
import { ShowIssuePlanComponent } from './show-issue-plan/show-issue-plan.component';
import { ListWorkProcessComponent } from './list-work-process/list-work-process.component';
import { StopWatchComponent } from './stop-watch/stop-watch.component';
import { DashboardListComponent } from './dashboard-list/dashboard-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoaderService } from './loader.service';

import { CookieService } from 'ngx-cookie-service';
import { IssuedPlanSearchComponent } from './issued-plan-search/issued-plan-search.component';
import { ShowIssuedPlanComponent } from './show-issued-plan/show-issued-plan.component';
import { ModalCheckMaterialComponent } from './modal-check-material/modal-check-material.component';
import { DashboardTableComponent } from './dashboard-table/dashboard-table.component';
import { MovejobComponent, MoveJobDialogComponent } from './movejob/movejob.component';
import { WorkProcessCmmService } from './work-process-cmm.service';
import { ConfirmLastJobComponent } from './confirm-last-job/confirm-last-job.component';

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'work-process', component: WorkProcessComponent },
  { path: 'issue-plan', component: IssuePlanComponent },
  { path: '', component: DashboardComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    WorkProcessInputComponent,
    ShowWorkProcessDetailComponent,
    WorkProcessComponent,
    ActionWorkProcessComponent,
    IssuePlanConfirmComponent,
    IssuePlanComponent,
    ShowIssuePlanComponent,
    ListWorkProcessComponent,
    StopWatchComponent,
    DashboardListComponent,
    DashboardComponent,
    IssuedPlanSearchComponent,
    ShowIssuedPlanComponent,
    ModalCheckMaterialComponent,
    DashboardTableComponent,
    MovejobComponent,
	MoveJobDialogComponent,
	ConfirmLastJobComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
	HttpModule,
	HttpClientModule,
	BrowserAnimationsModule,
	NoopAnimationsModule,
	NgbModule.forRoot(),
	NgxFitTextModule.forRoot(),
	RouterModule.forRoot(
		appRoutes, { 
			enableTracing: false, 
			useHash: false 
		}
	),
	MatFormFieldModule,
	MatTableModule,
	MatInputModule,
	MatIconModule,
	MatTabsModule,
	MatDividerModule,
	MatSlideToggleModule,
	MatCardModule,
	MatMenuModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatTooltipModule,
	MatRadioModule,
	FlexLayoutModule,
	MatDialogModule
  ],
  providers: [WorkProcessService, IssuePlanService, LoaderService, CookieService, NgbActiveModal, WorkProcessCmmService],
  entryComponents: [ModalCheckMaterialComponent, MoveJobDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
