import { Component, OnInit} from '@angular/core';
import { WorkProcessCmmService } from '../work-process-cmm.service';

@Component({
  selector: 'app-confirm-last-job',
  templateUrl: './confirm-last-job.component.html',
  styleUrls: ['./confirm-last-job.component.css']
})
export class ConfirmLastJobComponent implements OnInit {

	jobno: string;
	
	constructor(private workProcessCmmService: WorkProcessCmmService) { }

	ngOnInit() {
	}
	
	mark(event){
		console.log(event.target.value);
	}
	
	submitJobNo(){
		this.workProcessCmmService.submitConfirmLastJob(this.jobno);
	}
}
