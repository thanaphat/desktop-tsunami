import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmLastJobComponent } from './confirm-last-job.component';

describe('ConfirmLastJobComponent', () => {
  let component: ConfirmLastJobComponent;
  let fixture: ComponentFixture<ConfirmLastJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmLastJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmLastJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
