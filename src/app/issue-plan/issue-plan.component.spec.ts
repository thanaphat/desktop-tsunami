import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuePlanComponent } from './issue-plan.component';

describe('IssuePlanComponent', () => {
  let component: IssuePlanComponent;
  let fixture: ComponentFixture<IssuePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
