import { Component, Input, Output, EventEmitter, OnInit  } from '@angular/core';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-issue-plan',
  templateUrl: './issue-plan.component.html',
  styleUrls: ['./issue-plan.component.css'],
  providers: [LoaderService]
})
export class IssuePlanComponent implements OnInit {
	machine: string = null;
	date: string = null;
	dateFormat: string = null;
	shift: string = null;
	
	dateIssued: string = null;
	machineIssued: string = null;
	dateFormatIssued: string = null;
	
	showLoader: boolean = false;

	constructor(private loaderService: LoaderService) { }
	
	ngOnInit() {
        this.loaderService.status.subscribe((val: boolean) => {
            this.showLoader = val;
        });
    }
}