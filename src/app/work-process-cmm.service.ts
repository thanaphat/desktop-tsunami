import { Injectable, Output, EventEmitter  } from '@angular/core';
import { WorkProcessService } from './work-process.service';
import { WorkProcessModel, ListWorkProcessModel } from './work-process.model';

@Injectable()
export class WorkProcessCmmService {
  
  @Output('')
  change: EventEmitter<string> = new EventEmitter();
  
  @Output('')
  clear: EventEmitter<boolean> = new EventEmitter();
  
  @Output('')
  objWorkProcessModel: EventEmitter<WorkProcessModel> = new EventEmitter();

  constructor(private workProcessService: WorkProcessService) { }
  
  submitMoveJob(_jobno: string){
	this.change.emit(_jobno);
  }
  
  clearState(){
	this.clear.emit(true);
  }
  
  submitConfirmLastJob(_jobno: string){
	this.workProcessService
		.postJobNo(_jobno)
		.subscribe(
			result => this.objWorkProcessModel.emit(result[0]),
			error => console.log("Error :: " + error)
		);
  
  }
}
