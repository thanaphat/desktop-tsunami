import { Component, OnInit, Input,
	Output, EventEmitter, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
import { WorkProcessService } from '../work-process.service';
import { WorkProcessModel, ProductionTransactionModel } from '../work-process.model';
import { MatDialog } from '@angular/material';
import { ModalCheckMaterialComponent } from '../modal-check-material/modal-check-material.component';


import * as moment from 'moment';

@Component({
  selector: 'app-action-work-process',
  templateUrl: './action-work-process.component.html',
  styleUrls: ['./action-work-process.component.css']
})
export class ActionWorkProcessComponent {

	start_prgid: string = "Start";
	stop_prgid: string = "Stop";
	resume_prgid: string = "Resume";
	
	fg_qty:	number = 0;
	ng_qty:	number = 0;
	selectedItem: WorkProcessModel = new WorkProcessModel();
	listMatUse: WorkProcessModel[] = [];
	
	clickStart: boolean = false;
	clickFinish: boolean = false;
	
	stopWatchStage: string = this.stop_prgid;
	startTimer: number = 0;
	stateBreak: boolean = false;
	isConfirmMat: boolean = false;
	
	jobno: string;
	
	@Input('item')
	set item(item: WorkProcessModel){
		if(item != null){
			this.selectedItem = item;
			this.jobno = this.selectedItem.jobno;
			this.clickStart = false;
			if(this.selectedItem.prc_start != null){
				this.resumeWorkProcess(this.selectedItem, this.resume_prgid);
			}
		}
	}
	
	@Input('emp_no')
	emp_no: string = null;
	
	@Output('processStatus')
	processStatus: EventEmitter<WorkProcessModel> = new EventEmitter();
	
	constructor(private workProcessService: WorkProcessService
			, private dialog: MatDialog){}
	
	@HostListener('window:beforeunload', ['$event'])
	beforeUnloadHander(event) {
		return this.stopWatchStage == "Start" ? false : true;
	}
	
	resumeWorkProcess(item: WorkProcessModel, prgid: string){
		this.clickStart = true;
		this.startTimer = this.calDiffTime(item.prc_start);
		this.stopWatchStage = this.resume_prgid;
	}
	
	clearWorkProcess(){
		this.fg_qty = 0;
		this.ng_qty = 0;
		this.processStatus.emit(new WorkProcessModel());
		this.stopWatchStage = "Reset";
		
		this.clickStart = false;
		this.clickFinish = false;
		this.startTimer = 0;
		this.selectedItem = null;
		this.isConfirmMat = false;
		this.listMatUse.length = 0;
	}
	
	startWorkProcess(item: WorkProcessModel, prgid: string){
		console.log("startWorkProcess :: on");
		
		this.clickStart = true;
		let param1: string = this.formatStart(item);
		return this.workProcessService
			.postWPStart(param1, prgid)
			.subscribe(
				resultArray => {
					console.log("Return from service :: " + resultArray['result']);
					this.stopWatchStage = prgid;
				},
				error => console.log("Error :: " + error)
			);
	}
	
	private formatStart(item: WorkProcessModel): string {
		item.emp_cd = this.emp_no;
		let param1: string = item.i_po_detail_no.trim() + "!" + item.i_sim_item_cd.trim() + "!" + item.emp_cd.trim() + "!" + item.cur_station.trim() + "!" + item.next_station.trim() + "!" + item.jobno.trim();
		return param1.trim();
	}
	
	stopWorkProcess(item: WorkProcessModel, prgid: string){
		this.clickFinish = true;
		
		let param1 = this.formatStop(item);
		param1 = param1 + "!" + this.fg_qty.toString() + "!" + this.ng_qty.toString();
		return this.workProcessService
			.postWPStop(param1, prgid)
			.subscribe(
				resultArray => {
					this.stopWatchStage = prgid;
				},
				error => console.log("Error :: " + error),
				() => {
					console.log("stopWorkProcess success :: ");
					/*
					Insert all material
					*/
					this.listMatUse.forEach((e, i) => {
						this.insertItemTransaction(e, i, 1);
					});
					
					this.insertItemTransaction(item, 1, 0);
				}
			);
	}
	
	private formatStop(item: WorkProcessModel): string {
		item.emp_cd = this.emp_no;
		let param1: string = item.i_po_detail_no.trim() + "!" + item.i_sim_item_cd.trim() + "!" + item.emp_cd.trim() + "!" + item.cur_station.trim() + "!" + item.next_station.trim() + "!" + item.jobno.trim();
		return param1.trim();
	}
	
	private calDiffTime(time: string): number{
		let now = moment();
		let end = moment(time);
		var duration = moment.duration(now.diff(end));
		var seconds = duration.asSeconds();
		
		return Math.round(seconds);
	}
	
	toggleBreak(jobno: string, emp_no: string){
		this.stateBreak = !this.stateBreak;
		
		let typeToggle: string = (this.stateBreak) ? 'C11' : 'C12';
		this.processState(jobno, typeToggle, emp_no);
	}
	
	processState(jobno: string, type: string, emp: string){
		return this.workProcessService
				.postProcessState(jobno, type, emp)
				.subscribe(
					resultArray => {
						console.log("Return from service :: " + resultArray['result']);
					},
					error => console.log("Error :: " + error)
				);
	}
	
	checkDisableStart(){
		return (JSON.stringify(this.selectedItem)) == '{}' || this.clickStart || (this.emp_no == null || this.emp_no.trim() == '') || !this.isConfirmMat;
	}
	
	checkDisableInputQty(){
		return (JSON.stringify(this.selectedItem)) == '{}' || !this.clickStart || this.stateBreak;
	}
	
	checkDisableFinish(){
		return (JSON.stringify(this.selectedItem)) == '{}' || !this.clickStart || this.clickFinish || (this.emp_no == null || this.emp_no.trim() == '') || this.stateBreak;
	}
	
	checkDisableClear(){
		return this.selectedItem == null || (this.clickStart && this.clickFinish == false) || this.stateBreak;
	}
	
	checkDisableCheckMaterial(){
		return (JSON.stringify(this.selectedItem)) == '{}' || this.clickStart;
	}
	
	openDialog(){
		let dialogRef = this.dialog.open(ModalCheckMaterialComponent, {
			closeOnNavigation: true,
			disableClose: true,
			data: { 
				i_sim_item_cd: this.selectedItem.i_sim_item_cd.trim(),
				i_fac_cd: this.selectedItem.i_fac_cd.trim(),
				listMatUse: this.listMatUse
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
			if(result != null && result.length > 0){
				this.isConfirmMat = true;
				this.listMatUse = result;
				console.log(result);
			}
		});
	}
	
	private insertPdTr(tr: ProductionTransactionModel){
		return this.workProcessService
			.postInsertPdTransaction(tr)
			.subscribe(
				resultBoolean => {
					console.log(resultBoolean);
					return resultBoolean[0].result;
				}
			);
	}
	
	private insertItemTransaction(list: WorkProcessModel, index: number, iotype: number){
		let tr: ProductionTransactionModel = {
			stkprd: moment().format('YYYYMM'),
			whcd: list.cur_station,
			otpartycd: list.cur_station,
			item_cd: list.i_sim_item_cd,
			customer: list.i_sch_lot_no.split("_")[0],
			cus_partno: list.i_plan_remark1,
			packaging: '',
			doctype: '03',
			iotype: iotype,
			ioqty: list.i_po_qty,
			lotno: list.jobno,
			planno: list.jobno,
			refno1: list.jobno,
			refno1_no: index,
			remark: 'MarkIT Test Api',
			insprgid: 'WebProcess',
			insusrid: this.emp_no
		};
		
		this.insertPdTr(tr);
	}
	
}
