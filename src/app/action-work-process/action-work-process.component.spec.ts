import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWorkProcessComponent } from './action-work-process.component';

describe('ActionWorkProcessComponent', () => {
  let component: ActionWorkProcessComponent;
  let fixture: ComponentFixture<ActionWorkProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWorkProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWorkProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
