import { Component, OnChanges, OnInit,
		Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { WorkProcessService } from '../work-process.service';
import { WorkProcessModel } from '../work-process.model';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-show-work-process-detail',
  templateUrl: './show-work-process-detail.component.html',
  styleUrls: ['./show-work-process-detail.component.css'],
  providers: [WorkProcessService]
})
export class ShowWorkProcessDetailComponent implements OnChanges, OnInit{
	
	_postWorkProcessArray: WorkProcessModel[];
	currentProcess: string;
	emp_cd: string;
	selectedItem: WorkProcessModel;
	rowSelected: number;
	confirmItem: boolean = false;
	_loading: boolean = false;
	
	@Input('isOnProcess')
	isOnProcess: boolean = false;
	
	constructor(private workprocessService: WorkProcessService, private loaderService: LoaderService){
		this._postWorkProcessArray = new Array<WorkProcessModel>();
		this.rowSelected = null;
		this.selectedItem = null;
		this.confirmItem = false;
	}
	
	@Input('jobNo')
	jobNo: string = '-';
	
	ngOnInit(){
	}
	
	ngOnChanges(changes: SimpleChanges) {
		for (let propName in changes) {  
			let change = changes[propName];
		
			/*
			let curVal  = JSON.stringify(change.currentValue);
			let prevVal = JSON.stringify(change.previousValue);
			let changeLog = `${propName}: currentValue = ${curVal}, previousValue = ${prevVal}`;
			console.log(changeLog);
			*/
		}
		
		this._postWorkProcessArray = new Array<WorkProcessModel>();
		this.rowSelected = null;
		this.selectedItem = null;		
		this.confirmItem = false;

		if(this.jobNo != '-' && !this._loading)
			this.searchByJobNo(this.jobNo);
	}

	@Output('objectSelected')
	objectSelected: EventEmitter<WorkProcessModel> = new EventEmitter();
	
	chooseItem(){
		this.confirmItem = true;
		this.objectSelected.emit(this.selectedItem);
	}
	
	chooseListItem(event, list: WorkProcessModel, index: number){
		if(event.target.checked){
			this.setSelectedItem(list, index);
		}else{
			this.rowSelected = null;
			this.selectedItem = null;
		}
	}
	
	getSelectedItem(): WorkProcessModel{
		return this.selectedItem;
	}
	
	searchByJobNo(jobNo: string): void{
		if(this.loaderService.status.getValue() == false)
			this.loaderService.display(true);
		
		this._loading = true;
		
		this.workprocessService
			.postJobNo(jobNo)
			.subscribe(
				resultArray => {
					this._postWorkProcessArray = resultArray;
					if(this._postWorkProcessArray.length == 1){
						this.setSelectedItem(this._postWorkProcessArray[0], 0);
						
						if(this.isOnProcess === true)
							this.chooseItem();
					}else{
						/*
						if(this.isOnProcess === true)
							this.setSelectedItem(this._postWorkProcessArray[0], 0);
							//this.chooseItem();
						*/
					}
					
					this.loaderService.display(false);
					
					this._loading = false;
				},
				error => {
					console.log("Error :: " + error);
					this.loaderService.display(false);
					this._loading = false;
				}
		);
	}
	
	setSelectedItem(list: WorkProcessModel, index: number){
		this.selectedItem = list;
		this.rowSelected = index;
	}
}