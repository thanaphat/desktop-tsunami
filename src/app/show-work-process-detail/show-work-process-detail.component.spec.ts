import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowWorkProcessDetailComponent } from './show-work-process-detail.component';

describe('ShowWorkProcessDetailComponent', () => {
  let component: ShowWorkProcessDetailComponent;
  let fixture: ComponentFixture<ShowWorkProcessDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowWorkProcessDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowWorkProcessDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
