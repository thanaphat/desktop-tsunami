import { Component, OnInit, OnChanges, SimpleChanges,
	ViewChild} from '@angular/core';
import { WorkProcessService } from '../work-process.service';
import { WorkProcessModel } from '../work-process.model';
import { LoaderService } from '../loader.service';
import { CookieService } from 'ngx-cookie-service';
import { ListWorkProcessComponent } from '../list-work-process/list-work-process.component';
import { WorkProcessCmmService } from '../work-process-cmm.service';

@Component({
  selector: 'app-work-process',
  templateUrl: './work-process.component.html',
  styleUrls: ['./work-process.component.css']
})
export class WorkProcessComponent implements OnInit, OnChanges{
	wono: string = null;
	indContent: string = null;
	jobNo: string = '-';
	isOnProcess: boolean = false;
	selectedItem: WorkProcessModel = new WorkProcessModel();
	emp_no: string = null;
	_cookieEmpNo: string;
	
	@ViewChild(ListWorkProcessComponent) listChild: ListWorkProcessComponent;

	constructor(private workprocessService: WorkProcessService
			, private loaderService: LoaderService
			, private cookieService: CookieService
			, private workProcessCmmService: WorkProcessCmmService){
		this.selectedItem = new WorkProcessModel();
	}
	
	ngOnInit(){
		this.emp_no = this.getCookie('emp_no');
		this.searchJobNo();		/* Get job on process	*/
		
		this.workProcessCmmService.change.subscribe(d => {
			this.isOnProcess = true;
			this.setJobNo(d);
		});
		
	}
	
	getOutputWoNo(value){
		this.wono = value;
	}
	
	setCookie(key: string, value: string){
		if(value.trim() == ''){
			this.deleteCookie(key);
			return;
		}
			
		this.cookieService.set(key, value);
	}
	
	deleteCookie(key: string){
		this.cookieService.delete(key);
	}
	
	getCookie(key: string): string{
		return this.cookieService.get(key);
	}
	
	
	searchJobNo(): void{
		this.workprocessService
			.getJobOnProcess()
			.subscribe(
				resultArray => {
					if(resultArray.length > 0){
						this.jobNo = resultArray[0]['jobno'];
						this.emp_no = resultArray[0]['emp_cd'];
						this.isOnProcess = true;
					}else{
						this.jobNo = '-';
						this.isOnProcess = false;
					}
				},
				error => {
					console.log("Error :: " + error);
					this.isOnProcess = false;
				}
		);
	}
	
	ngOnChanges(changes: SimpleChanges) {
	}
	
	searchByJobNo(jobNo: string): void{		
		this.workprocessService
			.postJobNo(jobNo)
			.subscribe(
				resultArray => {
					this.getOutputWPModel(resultArray[0]);
				},
				error => {
					console.log("Error :: " + error);
				}
		);
	}
	
	getOutputWPModel(value){
		this.selectedItem = value;
	}
	
	getSelectedItem(): WorkProcessModel{
		return this.selectedItem;
	}
	
	getStateClear(value){
		this.selectedItem = new WorkProcessModel();
		this.jobNo = '-';
		this.listChild.updateListWork();
		this.isOnProcess = false;
		this.listChild.jobSearch = null;
		this.searchJobNo();
		
		this.workProcessCmmService.clearState();
	}
	
	setJobNo(value){
		this.jobNo = value;
	}
}
