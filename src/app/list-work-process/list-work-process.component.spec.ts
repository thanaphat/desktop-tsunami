import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWorkProcessComponent } from './list-work-process.component';

describe('ListWorkProcessComponent', () => {
  let component: ListWorkProcessComponent;
  let fixture: ComponentFixture<ListWorkProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWorkProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWorkProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
