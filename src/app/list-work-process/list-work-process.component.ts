import { Component, Input, OnChanges, SimpleChanges,
		OnInit, Output, EventEmitter } from '@angular/core';
import { ListWorkProcessModel, WorkProcessModel } from '../work-process.model';
import { IntervalObservable } from "rxjs/observable/IntervalObservable";
import { WorkProcessService } from '../work-process.service';
import { WorkProcessCmmService } from '../work-process-cmm.service';
import { LoaderService } from '../loader.service';

import * as moment from 'moment';

@Component({
  selector: 'app-list-work-process',
  templateUrl: './list-work-process.component.html',
  styleUrls: ['./list-work-process.component.css']
})
export class ListWorkProcessComponent implements OnInit {

	_originListIssued: ListWorkProcessModel[];
	_listIssuedPlanArray: ListWorkProcessModel[] = new Array<ListWorkProcessModel>();
	private _loading: boolean = false;
	currentDate: string = null;
	currentDateFormat: string = null;
	currentShift: string = 'Day';
	issueSelected: ListWorkProcessModel = null;
	jobSearch: string = null;
	
	@Input('_confirmItem')
	_confirmItem: WorkProcessModel = null;
	
	@Output('jobNo')
	jobNo: EventEmitter<string> = new EventEmitter();
	
	constructor(private workProcessService: WorkProcessService
		, private loaderService: LoaderService
		, private workProcessCmmService: WorkProcessCmmService
	) { }
	
	ngOnInit(){
		this.updateListWork();
	}
	
	updateListWork(){
		this.setDateTime();
		this.searchPlanByMachine(this.currentDate);
	}
	
	chooseList(list: ListWorkProcessModel){
		this.issueSelected = list;
		
		this.jobNo.emit(list.jobno);
	}
	
	searchPlanByMachine(date: string){
		this._loading = true;
		
		return this.workProcessService
			.postIssuedPlanByMachine(date)
			.subscribe(
				resultArray => {
					this._loading = false;
					this._listIssuedPlanArray = resultArray;
					this._originListIssued = resultArray;
				},
				error => {
					console.log("Error :: " + error);
					
					this._loading = false;
				}
			);
	}
	
	filterJobList(){
		this._listIssuedPlanArray = this._originListIssued;
		if(this.jobSearch.trim() != ''){
			this._listIssuedPlanArray = this._listIssuedPlanArray.filter(data => {			
				return data.jobno.toLowerCase().indexOf(this.jobSearch.toLowerCase()) > -1 ? true : false;
			});
		}
	}
	
	print(){	
		let printContents, popupWin;
		printContents = document.getElementById('print-section').innerHTML;
		popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		popupWin.document.open();
		popupWin.document.write(`
			<html>
				<head>
					<title>Print tab</title>
					<style>
						caption {
							padding-top: 0.75rem;
							padding-bottom: 0.75rem;
							color: #6c757d;
							text-align: left;
							caption-side: bottom;
						}
						.card-header {
							padding: 0.75rem 1.25rem;
							margin-bottom: 0;
							background-color: rgba(0, 0, 0, 0.03);
							border-bottom: 1px solid rgba(0, 0, 0, 0.125);
						}
						.card-header:first-child {
							border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
						}
						h5, .h5 {
							font-size: 1.25rem;
						}
						.float-md-left {
							float: left !important;
						}
						table {
							border-collapse: collapse;
						}
						
						.table {
							width: 100%;
							max-width: 100%;
							margin-bottom: 1rem;
							background-color: transparent;
						}
						
						.table-bordered th, .table-bordered td {
							border: 1px solid #dee2e6;
						}
						
						.table-sm th, .table-sm td {
							padding: 0.3rem;
						}
						
						.table-bordered {
							border: 1px solid #dee2e6;
						}
						
						.text-center {
							text-align: center !important;
						}
						
						td, th {
							display: table-cell;
							vertical-align: inherit;
						}
					</style>
				</head>
				<body onload="window.print();window.close()">${printContents}</body>
			</html>`
		);
		popupWin.document.close();
	}

	private setDateTime(){
		let current = moment();
		let dayStartTime = moment('08:00:00', 'HH:mm:ss');
		let dayEndTime = moment('20:10:00', 'HH:mm:ss');
		
		let shiftStartTime = moment('20:00:00', 'HH:mm:ss');
		let midNightTime = moment('23:59:59', 'HH:mm:ss');
		let currDate: any = '';
		
		if(current.isBetween(dayStartTime, dayEndTime)){
			this.currentShift = "Day";
			currDate = current;
		}else{
			this.currentShift = "Night";

			if(current.isBetween(shiftStartTime, midNightTime))
				currDate = current;
			else
				currDate = moment().add(-1, 'days');
		}
		
		this.currentDate = currDate.format('YYYYMMDD');
		this.currentDateFormat = currDate.format('YYYY-MM-DD');
	}
}
