import { TestBed, inject } from '@angular/core/testing';

import { WorkProcessCmmService } from './work-process-cmm.service';

describe('WorkProcessCmmService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProcessCmmService]
    });
  });

  it('should be created', inject([WorkProcessCmmService], (service: WorkProcessCmmService) => {
    expect(service).toBeTruthy();
  }));
});
