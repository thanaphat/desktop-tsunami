import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkProcessInputComponent } from './work-process-input.component';

describe('WorkProcessInputComponent', () => {
  let component: WorkProcessInputComponent;
  let fixture: ComponentFixture<WorkProcessInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkProcessInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkProcessInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
