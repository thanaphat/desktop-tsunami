import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-work-process-input',
  templateUrl: './work-process-input.component.html',
  styleUrls: ['./work-process-input.component.css']
})
export class WorkProcessInputComponent{
	wo_no: string = "";
	formSubmitted: boolean = false;
	
	@Output() submitWoNo: EventEmitter<string> = new EventEmitter();
	
	constructor(){}
	
	submitForm(form: NgForm){
		this.formSubmitted = true;
		if(form.valid){
			this.submitWoNo.emit(this.wo_no);
			form.reset();
			this.formSubmitted = false;
		}
	}
}
