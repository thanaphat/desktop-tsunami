import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	showLoader: boolean = false;

	constructor(private loaderService: LoaderService) { }

	ngOnInit() {
		this.loaderService.status.subscribe((val: boolean) => {
			this.showLoader = val;
		});
	}

}
