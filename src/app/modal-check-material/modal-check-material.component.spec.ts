import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCheckMaterialComponent } from './modal-check-material.component';

describe('ModalCheckMaterialComponent', () => {
  let component: ModalCheckMaterialComponent;
  let fixture: ComponentFixture<ModalCheckMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCheckMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCheckMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
