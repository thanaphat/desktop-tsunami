import { Component, OnInit, Inject, AfterViewInit, ChangeDetectorRef
	, AfterViewChecked} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import { WorkProcessService } from '../work-process.service';
import { WorkProcessModel, ListWorkProcessModel, PartStructureModel } from '../work-process.model';

@Component({
  selector: 'app-modal-check-material',
  templateUrl: './modal-check-material.component.html',
  styleUrls: ['./modal-check-material.component.css']
})
export class ModalCheckMaterialComponent implements OnInit, AfterViewInit, AfterViewChecked {

	_jobno: string;
	jobno: string[] = [];
	wip: string[] = [];
	material: string[] = [];
	_listMat: WorkProcessModel[] = [];
	i_item_cd_target: string;
	_listPs: PartStructureModel[] = [];
	_listMatUse: WorkProcessModel[] = [];
	
	constructor(
		public dialogRef: MatDialogRef<ModalCheckMaterialComponent>
		, @Inject(MAT_DIALOG_DATA) public data: any
		, private workProcessService: WorkProcessService
		, private cdRef: ChangeDetectorRef) {
		
		this.i_item_cd_target = this.data.i_sim_item_cd.trim();
	}
	
	ngOnInit(){
		this._listPs.length = 0;
		
		this.workProcessService
			.postSearchPsByItemCd(this.data.i_fac_cd.trim(), this.data.i_sim_item_cd.trim())
			.subscribe(
				resultArray => {
					this._listPs = resultArray;
				},
				error => console.log("Error :: " + error),
				() => {
					let currentMatList = this.data.listMatUse;
					currentMatList.forEach(e => {
						let f: WorkProcessModel[] = this._listMat.filter(el => el.jobno.trim() == e.jobno.trim());
						if(f.length == 0){
							this._listMat.push(e);

							if(this.isMatInListPs(e)){
								this.addListMatUse(e);
							}
						}
						
					});
				}
			);		
	}
	
	ngAfterViewInit(){
	}
	
	ngAfterViewChecked(){
		this.cdRef.detectChanges();
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
	
	inputJobNo(value){
		return this.workProcessService
			.postJobNo(this._jobno.trim())
			.subscribe(
				resultArray => {
					resultArray.forEach(e => {
						let f: WorkProcessModel[] = this._listMat.filter(el => el.jobno.trim() == e.jobno.trim());
						if(f.length == 0){
							this._listMat.push(e);

							if(this.isMatInListPs(e)){
								this.addListMatUse(e);
							}
						}
						
					});
				},err => {}, () => this._jobno = null
			);
	}
	
	clickDeleteMat(mat: WorkProcessModel){
		this._listMat = this._listMat.filter(e => e.i_sim_item_cd.trim() != mat.i_sim_item_cd.trim() && e.jobno.trim() != mat.jobno.trim());
		
		let fIndex: number = this._listMatUse.findIndex(e => {
			return e.jobno.trim() == mat.jobno.trim()
		});
		
		this._listMatUse.splice(fIndex, 1);
	}
	
	addListMatUse(mat: WorkProcessModel){
		this._listMatUse.push(mat);
	}
	
	isMatInListPs(mat: WorkProcessModel): boolean{
		let copy_mat: PartStructureModel[] = this._listPs;
		let f = copy_mat.filter(e => e.i_lower_item_cd.trim() == mat.i_sim_item_cd.trim());
		
		return f.length ? true : false;
	}
	
	checkLength(): boolean{
		return this._listMatUse.length !== this._listPs.length;
	}
}