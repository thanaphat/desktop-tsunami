export class IssuePlanModel {
	seiban: string;
	i_sch_lot_no: string;
	i_plan_remark1: string;
	mc: string;
	line: string;
	shift: string;
	ind: string;
	i_sim_ind_content: string;
	i_po_detail_no: string;
	item_cd: string;
	i_sim_item_cd: string;
	i_sim_po_qty: string;
	seqno: number;
	state: string;
	
	i_shift: string;
	i_line: string;
	i_plan_ind_content: string;
	i_seiban: string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}