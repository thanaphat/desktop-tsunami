import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IssuePlanModel } from './issue-plan.model';

@Injectable()
export class IssuePlanService {
	
	_postIssuePlanByMachineUrl: string = 'http://10.152.18.240:3011/Machine';
	_postConfirmIssuePlanUrl: string = 'http://10.152.18.240:3011/Issue';
	_postIssuedPlanByMachineUrl: string = 'http://10.152.18.240:3111/jobstatus';
	
	constructor(private http: Http){}
	
	postIssuePlanByMachine(date: string, machine: string, shift: string): Observable<IssuePlanModel[]>{
		return this.http
			.post(this._postIssuePlanByMachineUrl, {
				dts: date != null ? date.trim() : null,
				mac: machine != null ? machine.trim() : null,
				shift: shift != null ? shift.trim() : null
			})
			.map(res => {
				console.log("Observable on (postIssuePlanByMachine) :: " + res.status);
				console.log("res :: " + res);
				return res.json();
			})
            .catch(this.handleError);
	}
	
	postConfirmIssuePlan(date: string, machine: string, shift: string): Observable<boolean>{
		return this.http
			.post(this._postConfirmIssuePlanUrl, {
				dts: date != null ? date.trim() : null,
				mac: machine != null ? machine.trim() : null,
				shift: shift != null ? shift.trim() : null
			})
			.map(res => {
				console.log("Observable on (postConfirmIssuePlan) :: " + res.status);
				console.log("res :: " + res);
				return res.json();
			})
            .catch(this.handleError);
	}
	
	postIssuedPlanByMachine(date: string, machine: string): Observable<IssuePlanModel[]>{
		return this.http
			.post(this._postIssuedPlanByMachineUrl, {
				dts: date != null ? date.trim() : null,
				mac: machine != null ? machine.trim() : null
			})
			.map(res => {
				console.log("Observable on (postIssuedPlanByMachine) :: " + res.status);
				console.log("res :: " + res);
				return res.json();
			})
            .catch(this.handleError);
	}
	
	private handleError(error: Response){
		return Observable.throw(error.statusText);
	}

}
