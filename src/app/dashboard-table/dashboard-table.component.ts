import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { WorkProcessService } from '../work-process.service';
import { Dashboard, JobOutput, TimeProcess } from '../dashboard.model';
import { Observable, Subscription } from 'rxjs';
import { LoaderService } from '../loader.service';


import * as moment from 'moment';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.css']
})
export class DashboardTableComponent implements OnInit, AfterViewInit {

	_postDashboardItem: Dashboard[] = new Array<Dashboard>();
	currentShift: string;
	currentDateFormat: string;
	currentDate: string;	/*	as WO Date	*/
	currentDateMoment: any;
	
	currentStart: string;
	currentEnd: string;
	
	dataSource = new MatTableDataSource();
	
	displayedColumnsDay: string[] = ['M/C', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00'];
	displayedColumnsNight: string[] = ['M/C', '20:00', '21:00', '22:00', '23:00', '00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00'];
	_minDayHour: string = this.displayedColumnsDay[1];
	_maxDayHour: string = this.displayedColumnsDay[this.displayedColumnsDay.length-1];
	_minNightHour: string = this.displayedColumnsNight[1];
	_maxNightHour: string = this.displayedColumnsNight[this.displayedColumnsNight.length-1];
	displayedColumns: string[];
	
	sub: Subscription;
	timer = Observable.timer(0, 60000);
	
	constructor(private workProcessService: WorkProcessService, private loaderService: LoaderService) { }

	ngOnInit() {
		this.sub = this.timer.subscribe(() => {
			this.setDateTime();
			this.postProductionOutput(this.currentStart, this.currentEnd);
		});
	}
	
	ngAfterViewInit(){
	}

	postProductionOutput(dateS: string, dateE: string) {
		this.loaderService.display(true);
		
		return this.workProcessService
			.postMCStatus(dateS, dateE)
			.subscribe(
				result => {
					this._postDashboardItem = [];
					this.setJsontoFormatDashboardInterface(result);
					this.dataSource.data = this._postDashboardItem;
					
					this.loaderService.display(false);
				},
				error => {
					console.log("Error :: " + error);
					this.loaderService.display(false);
				}
			);
	}
	
	private setDateTime(){
		let current = moment();
		let dayStartTime = moment('08:00:00', 'HH:mm:ss');
		let dayEndTime = moment('20:10:00', 'HH:mm:ss');
		let nightStartTime = moment('20:00:00', 'HH:mm:ss');
		let midNightTime = moment('23:59:59', 'HH:mm:ss');
		let currDate: any = '';
		let minHour: any = '';
		
		this.currentEnd = current.format('YYYY-MM-DD HH:mm:ss').toString();
		this.currentDateMoment = current;
		this.currentDate = current.format('YYYYMMDD');
		this.currentDateFormat = current.format('YYYY-MM-DD HH:mm:ss');
		
		
		if(current.isBetween(dayStartTime, dayEndTime)){
			this.currentShift = "Day";
			minHour = this._minDayHour;
			currDate = current;
		}else{
			this.currentShift = "Night";
			minHour = this._minNightHour;
			
			if(current.isBetween(nightStartTime, midNightTime))				
				currDate = current;
			else
				currDate = current.add(-1, 'days');
		}
		
		this.displayedColumns = (this.currentShift == 'Day') ? this.displayedColumnsDay : this.displayedColumnsNight;
		
		this.currentStart = moment(currDate.format('YYYY-MM-DD') + ' ' + minHour, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm:ss');
	}
	
	switchLastShift(event){
		this.sub.unsubscribe();
		
		if(event.value == 'current'){
			this.sub = this.timer.subscribe(() => {
				this.setDateTime();
				this.postProductionOutput(this.currentStart, this.currentEnd);
			});
		}else if(event.value == 'last'){
			let targetShift: string;
			let targetMinHour: string;
			let targetMaxHour: string;
			let addDay: number = 0;
			let current = moment();
			
			if(this.currentShift == 'Day'){
				targetShift = 'Night';
				targetMinHour = this._minNightHour;
				targetMaxHour = this._maxNightHour;
				addDay = -1;
			}else{
				let nightStartTime = moment('20:00:00', 'HH:mm:ss');
				let midNightTime = moment('23:59:59', 'HH:mm:ss');

				if(current.isBetween(nightStartTime, midNightTime))				
					addDay = 0;
				else
					addDay = -1;
					
				targetShift = 'Day';
				targetMinHour = this._minDayHour;
				targetMaxHour = this._maxDayHour;
			}

			this.displayedColumns = (targetShift == 'Day') ? this.displayedColumnsDay : this.displayedColumnsNight;
			let currentDate = current.format('YYYY-MM-DD');
			
			let targetStartTime = moment(currentDate + ' ' + targetMinHour, 'YYYY-MM-DD HH:mm').add(addDay, 'days');
			let targetEndTime = moment(currentDate + ' ' + targetMaxHour, 'YYYY-MM-DD HH:mm');
			
			this.postProductionOutput(targetStartTime.format('YYYY-MM-DD HH:mm:ss'), targetEndTime.format('YYYY-MM-DD HH:mm:ss'));
		}
		console.log("switchLastShift :: " + event.value);
	}

	private setJsontoFormatDashboardInterface(array: any[]){
		let currentMC: string = '';
		let mainObj: Dashboard = new Dashboard();
		let resultObj = {};
		
		array.forEach((obj, i) => {
			
			if(currentMC == obj.mcp){
				let tmpHour: string = ((obj.h_start != null && obj.h_start.trim() != '') && obj.state === 'On Process') ? obj.h_start: obj.h_end;

				resultObj[tmpHour] = (resultObj[tmpHour] == null) ? [] : resultObj[tmpHour];
				resultObj[tmpHour].push(
					new JobOutput({
						jobno: obj.jobno,
						i_po_detail_no: obj.i_po_detail_no,
						i_sim_po_qty: obj.i_sim_po_qty,
						client_name: obj.client_name,
						time: new TimeProcess({
							prc_start: (obj.prc_start != null) ? moment(obj.prc_start).format('YYYY-MM-DD HH:mm:ss') : null,
							prc_end: (obj.prc_end != null) ? moment(obj.prc_end).format('YYYY-MM-DD HH:mm:ss') : null,
							h_start: obj.h_start,
							h_end: obj.h_end,
							state: obj.state
						})
					})
				);
				
			}else{
				if(i != 0){
					mainObj.result = resultObj;
					this._postDashboardItem.push(mainObj);
					//console.log("this._postDashboardItem :: " + JSON.stringify(this._postDashboardItem[0].result["15:00"][0]["jobno"]));
					
					mainObj = new Dashboard();
					resultObj = {};
				}
				
				let tmpHour: string = ((obj.h_start != null && obj.h_start.trim() != '') && obj.state === 'On Process') ? obj.h_start: obj.h_end;
				
				mainObj = new Dashboard({
					mcp: obj.mcp,
					date: this.currentDate,
					shift: this.currentShift
				});
				
				resultObj[tmpHour] = [];
				resultObj[tmpHour].push(new JobOutput({
						jobno: obj.jobno,
						i_po_detail_no: obj.i_po_detail_no,
						i_sim_po_qty: obj.i_sim_po_qty,
						client_name: obj.client_name,
						time: new TimeProcess({
							prc_start: (obj.prc_start != null) ? moment(obj.prc_start).format('YYYY-MM-DD HH:mm:ss') : null,
							prc_end: (obj.prc_end != null) ? moment(obj.prc_end).format('YYYY-MM-DD HH:mm:ss') : null,
							h_start: obj.h_start,
							h_end: obj.h_end,
							state: obj.state
						})
					})
				);

				currentMC = obj.mcp.trim();
			}
		});
		
		/*
		Push for the last object
		*/
		mainObj.result = resultObj;
		this._postDashboardItem.push(mainObj);
	}
}