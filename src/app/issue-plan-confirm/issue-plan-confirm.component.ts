import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-issue-plan-confirm',
  templateUrl: './issue-plan-confirm.component.html',
  styleUrls: ['./issue-plan-confirm.component.css']
})
export class IssuePlanConfirmComponent {
	model;
	p_machine: string = null;
	p_date: string = null;
	p_shift: string = null;
	formSubmitted: boolean = false;
	machine: Machine[] = MACHINE_DATA;
	
	@Output('machine')
	out_machine: EventEmitter<string> = new EventEmitter();
	
	@Output('date')
	out_date: EventEmitter<string> = new EventEmitter();
	
	@Output('dateFormat')
	out_dateFormat: EventEmitter<string> = new EventEmitter();
	
	@Output('shift')
	out_shift: EventEmitter<string> = new EventEmitter();
	
	constructor() { }
	
	get jsonDatePicker(){
		return JSON.stringify(this.model);
	}
	
	private sliceString(str: string, num: number){
		return str.slice(num);
	}
	
	onSubmit(form: NgForm){
		
		this.formSubmitted = true;
		if(form.valid){
			let day: string = this.sliceString('0' + form.value.p_date.day.toString(), -2);
			let month: string = this.sliceString('0' + form.value.p_date.month.toString(), -2);
			let year: string = form.value.p_date.year.toString();
			let date: string = year + month + day;
			let dateFormat: string = year + '-' + month + '-' + day;
			
			this.out_machine.emit(form.value.p_machine);
			this.out_date.emit(date);
			this.out_dateFormat.emit(dateFormat);
			this.out_shift.emit(form.value.p_shift);

			form.reset();
			this.formSubmitted = false;
		}
	}
}

export interface Machine{
	mcp: string;
	ind: string;
}

const MACHINE_DATA: Machine[] = [
	{mcp: "VerNo.1", ind: "VCNO1"},
	{mcp: "VerNo.2", ind: "VCNO2"},
	{mcp: "Rollcut.3", ind: "RC"},
	{mcp: "RWNo.2",ind: "RW002"},
	{mcp: "40TNo.1",ind: "P401"},
	{mcp: "40Ton",ind: "P4023"},
	{mcp: "40PRO",ind: "P40PR"},
	{mcp: "50ton",ind: "P50T"},
	{mcp: "55Ton",ind: "P55T"},
	{mcp: "60Ton",ind: "P60T2"},
	{mcp: "60TonNo.2",ind: "P60T"},
	{mcp: "80Ton",ind: "P80T"},
	{mcp: "Slitter.4",ind: "SL4"},
	{mcp: "100TonNo1",ind: "P1001"},
	{mcp: "100TonNo2",ind: "P1002"},
	{mcp: "200ton",ind: "P200T"},
	{mcp: "20Ton",ind: "P20T"},
	{mcp: "SlitterNo.1",ind: "SL1"},
	{mcp: "Supercut.1",ind: "SUPC1"},
	{mcp: "Supercut.4",ind: "SUPC4"},
	{mcp: "Cuttingsheet.1",ind: "SC"},
	{mcp: "SealPress",ind: "SP"},
	{mcp: "Primer",ind: "ELAMP"},
	{mcp: "LaminateNo.7",ind: "ELAM7"},
	{mcp: "LaminateDirectCoat",ind: "DC"},
	{mcp: "ASSY.G",ind: "ASSY-G"},
	{mcp: "ASSY.J",ind: "ASSY-J"},
	{mcp: "HW",ind: "HW"},
	{mcp: "HM",ind: "HW"},
	{mcp: "VACUMM",ind: "VC"},
	{mcp: "Ultra",ind: "UL"}
];

//{mcp: "SLIT",ind: "SL235"},