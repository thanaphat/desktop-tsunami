import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuePlanConfirmComponent } from './issue-plan-confirm.component';

describe('IssuePlanConfirmComponent', () => {
  let component: IssuePlanConfirmComponent;
  let fixture: ComponentFixture<IssuePlanConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuePlanConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuePlanConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
