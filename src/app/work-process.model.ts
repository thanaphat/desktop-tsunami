export class WorkProcessModel {
	i_po_detail_no: string;
	i_sim_item_cd: string;
	i_po_qty: number;
	i_plan_remark1: string;
	i_sch_lot_no: string;
	station: string;
	next_station: string;
	emp_cd: string;
	jobno: string;
	list_process: string;
	cur_station: string;
	prc_start: string;
	i_fac_cd: string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}

export class ListWorkProcessModel {
	machine: string;
	u_plan_date: string;
	u_ind_content: string;
	u_shift: string;
	u_revision: number;
	u_client: string;
	i_seiban: string;
	i_plan_ind_content: string;
	i_line: string;
	i_shift: string;
	i_pl_ind_content: string;
	next_station: string;
	i_po_detail_no: string;
	i_plan_item_cd: string;
	i_sim_item_cd: string;
	i_sim_po_qty: string;
	seqno: number;
	jobno: string;
	list_process: string;
	state: string;
	
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}

export class PartStructureModel{
	i_com_code: string;
	i_fac_cd: string;
	i_upper_item_cd: string;
	i_lower_item_cd: string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}

export class ProductionTransactionModel{
	stkprd: string;
	whcd: string;
	otpartycd: string;
	item_cd: string;
	customer: string;
	cus_partno: string;
	packaging: string;
	doctype: string;
	iotype: number;
	ioqty: number;
	lotno: string;
	planno: string;
	refno1: string;
	refno1_no: number;
	remark: string;
	insprgid: string;
	insusrid: string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}