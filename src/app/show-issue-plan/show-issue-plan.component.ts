import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { IssuePlanService } from '../issue-plan.service';
import { IssuePlanModel } from '../issue-plan.model';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-show-issue-plan',
  templateUrl: './show-issue-plan.component.html',
  styleUrls: ['./show-issue-plan.component.css']
})
export class ShowIssuePlanComponent implements OnChanges, OnInit {

	@Input('machine')
	machine: string = null;
	
	@Input('date')
	date: string = null;
	
	@Input('shift')
	shift: string = null;
	
	@Input('dateFormat')
	dateFormat: string = null;
	
	@Output('showLoader')
	showLoader: EventEmitter<boolean> = new EventEmitter();
	
	
	_postIssuePlanArray: IssuePlanModel[] = new Array<IssuePlanModel>();
	_confirmIssuePlan: boolean = false;
	_confirmRevision: number = null;
	alertClose: boolean = false;
	private _loading: boolean = false;
	
	constructor(private issuePlanService: IssuePlanService, private loaderService: LoaderService) {}
	
	ngOnInit(){
		this.searchIssuePlan(this.date, this.machine, this.shift);
	}
	
	ngOnChanges(){
		console.log("this.date :: " + this.date);
		console.log("this.machine :: " + this.machine);
		console.log("this.shift :: " + this.shift);
		
		this._postIssuePlanArray = new Array<IssuePlanModel>();
		this._confirmIssuePlan = false;
		this._confirmRevision = null;
		this.alertClose = false;
		
		this.searchIssuePlan(this.date, this.machine, this.shift);
	}

	confirmIssuePlan(date: string, machine: string, shift: string){
		this.loaderService.display(true);
		return this.issuePlanService
			.postConfirmIssuePlan(date, machine, shift)
			.subscribe(
				resultArray => {
					this._confirmRevision = resultArray['result'];
					this._confirmIssuePlan = true;
					console.log("_confirmRevision :: " + this._confirmRevision);
					//this.resetAll();
					this.loaderService.display(false);
				},
				error => console.log("Error :: " + error)
			);
	}
	
	searchIssuePlan(date: string, machine: string, shift: string){
		this.loaderService.display(true);
		this._loading = true;
		
		return this.issuePlanService
			.postIssuePlanByMachine(date, machine, shift)
			.subscribe(
				resultArray => {
					this._loading = false;
					this._postIssuePlanArray = resultArray;
					this.loaderService.display(false);
				},
				error => {
					this._loading = false;
					console.log("Error :: " + error);
				}
			);
	}
	
	resetAll(){
		this.machine = null;
		this.date = null;
		this.dateFormat = null;
		this.shift = null;
		
		this._postIssuePlanArray = new Array<IssuePlanModel>();
		this._confirmIssuePlan = false;
		this._confirmRevision = null;
		this.alertClose = false;
	}
}