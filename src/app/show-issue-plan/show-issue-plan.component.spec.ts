import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIssuePlanComponent } from './show-issue-plan.component';

describe('ShowIssuePlanComponent', () => {
  let component: ShowIssuePlanComponent;
  let fixture: ComponentFixture<ShowIssuePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowIssuePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIssuePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
