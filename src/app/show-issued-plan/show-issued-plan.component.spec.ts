import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIssuedPlanComponent } from './show-issued-plan.component';

describe('ShowIssuedPlanComponent', () => {
  let component: ShowIssuedPlanComponent;
  let fixture: ComponentFixture<ShowIssuedPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowIssuedPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIssuedPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
