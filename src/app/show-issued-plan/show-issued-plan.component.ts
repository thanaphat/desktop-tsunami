import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { IssuePlanService } from '../issue-plan.service';
import { IssuePlanModel } from '../issue-plan.model';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-show-issued-plan',
  templateUrl: './show-issued-plan.component.html',
  styleUrls: ['./show-issued-plan.component.css']
})
export class ShowIssuedPlanComponent implements OnInit, OnChanges {

	_postIssuedPlanArray: IssuePlanModel[] = new Array<IssuePlanModel>();
	_loading: boolean = false;

	@Input('machine')
	in_machine: string = null;
	
	@Input('date')
	in_date: string = null;
	
	@Input('dateFormat')
	in_dateFormat: string = null;
	
	@Output('showLoader')
	showLoader: EventEmitter<boolean> = new EventEmitter();
	
	constructor(private issuePlanService:IssuePlanService, private loaderService: LoaderService) { }

	ngOnInit() {
	}
	
	ngOnChanges(){
		this.searchIssuedPlan(this.in_date, this.in_machine);
	}
	
	searchIssuedPlan(date: string, machine: string){
		this._loading = true;
		this.loaderService.display(true);
		
		return this.issuePlanService
			.postIssuedPlanByMachine(date, machine)
			.subscribe(
				resultArray => {
					this._postIssuedPlanArray = resultArray;
					this.loaderService.display(false);
					this._loading = false;
				},
				error => {
					this._loading = false;
					console.log("Error :: " + error);
					this.loaderService.display(false);
				}
			);
	}

}
