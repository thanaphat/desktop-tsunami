import { TestBed, inject } from '@angular/core/testing';

import { WorkProcessService } from './work-process.service';

describe('WorkProcessService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProcessService]
    });
  });

  it('should be created', inject([WorkProcessService], (service: WorkProcessService) => {
    expect(service).toBeTruthy();
  }));
});
