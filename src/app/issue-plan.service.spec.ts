import { TestBed, inject } from '@angular/core/testing';

import { IssuePlanService } from './issue-plan.service';

describe('IssuePlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IssuePlanService]
    });
  });

  it('should be created', inject([IssuePlanService], (service: IssuePlanService) => {
    expect(service).toBeTruthy();
  }));
});
